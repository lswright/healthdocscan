<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>HealthDocScan</title>
    <script src="js/jquery.js"></script>
    <script src="javascript.js"></script>
<!--
    <script src="tether/src/js/utils.js"></script>
    <script src="tether/src/js/tether.js"></script>
-->
<!--    <script src="js/jqueryui/jquery-ui.js"></script>-->
    
    
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="styles.css">
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>
   <div class='container'>
      <center><h1>HealthDocScan</h1></center>
      <button id='selectpatientsbutton' type="button" class="btn btn-info" onclick='selectPatients()'>Hide/Show Patients</button>
      <button id='documentsshowbutton' type="button" class="btn btn-info" onclick='hideshowdocs()'>Hide/Show Documents</button>
                            <button id='uploadscans' type="button" class="btn btn-info" onclick='addscans()'>Add scans</button>
                            
       <div id='patientPanel'>
           <h3 id='selectpatientheading'>Select a patient</h3>
           <div id='patientList'>
<!--
              <div class='row patientRow' data-patientID=''>
               
                <div class='col'>
                  Patient ID
                </div>
                <div class='col-5'>
                  Neo Spider Luke
                </div>
                <div class='col'>
                  Address
                </div>
            </div> 
-->
           </div>
           
        <h3 id='selectdocumentheading'>Select a Document</h3>
       <div id='patienDocList'>
          
<!--
           <div class='row patientRow' data-patientID='' data-documentID=''>
               
                <div class="col">
                  Document ID
                </div>
                <div class="col-5">
                  document type
                </div>
                <div class="col">
                  document name
                </div>
            </div>
-->
           
       </div>
          
       <div id='documentcontents'>
           <h3>Content</h3>
           
       </div>
       <div id='addDocs'>
            <h3>Add scans</h3>
            
            
            
            <hr><div><div class="container">
                                        <div class="row">
                                          <div class="col-xs-12">
                                            <div class="panel panel-default">
                                              <div class="panel-body">
                                                <div class="progress">
                                                  <div class="progress-bar" role="progressbar"></div>
                                                </div>
                                                <button class="btn btn-lg upload-btn" type="button">Upload File</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <input id="upload-input" type="file" name="uploads[]" multiple="multiple"></br>
                                                                            </div><hr>
       </div>
   </div>
    
</body>
</html>