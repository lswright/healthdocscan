$(document).ready(function () {
    //Get all patients on load
    $.ajax({
                        url: "/healthdocscan/db.php"
                        , data:"getallpatients"
                        , type: "GET"
                        , success: function (thesepatients) {
                            
//                            console.log(thesepatients);
                            thesepatients = JSON.parse(thesepatients);
                            buildPatientList(thesepatients);
                        }
                    });
    
});

function buildPatientList(thesepatients){
    //build patient list on load
    var patientcount;
    
    for(patientcount=0;patientcount<thesepatients.length;patientcount++){
        
        var patientRow="<div class='row patientRow' data-patientID='"+thesepatients[patientcount].patientID+"' onclick='getthispatient(this)'><div class='col'>"+thesepatients[patientcount].patientID+"</div><div class='col-5'>"+thesepatients[patientcount].Name+"</div><div class='col'>"+thesepatients[patientcount].Address+"</div></div>"
        $('#patientList').append(patientRow);
     }
}

function getthispatient(thisPatientRow){
    //retrieve documents for a selected patient
    var patientID = thisPatientRow.getAttribute('data-patientID');
    data = {"getthispatient":"getthispatient","patientID":patientID}
     $.ajax({
                        url: "/healthdocscan/db.php"
                        , data:data
                        , type: "GET"
                        , success: function (thisPatientDocs) {
                            thisPatientDocs = JSON.parse(thisPatientDocs);
                            buildDocumentList(thisPatientDocs)
//                            console.log(thisPatientDocs);
                            
                        }
                    });  
}

function buildDocumentList(thisPatientDocs){
    //build list of documents for a selected patient
    if ($('.documentRow').length) {
        $('.documentRow').remove();
    }
    $('#selectdocumentheading:hidden').slideToggle();
    $('#patienDocList:hidden').slideToggle();
    var documentcount;
    for(documentcount = 0;documentcount<thisPatientDocs.length;documentcount++){
        var documentRow="<div class='row documentRow' data-patientID='"+thisPatientDocs[documentcount].patientID+"' data-documentID='"+thisPatientDocs[documentcount].documentID+"' data-patientID='"+thisPatientDocs[documentcount].docURL+"' onclick='getthisdocument(this)'><div class='col'>"+thisPatientDocs[documentcount].documentID+"</div><div class='col-5'>"+thisPatientDocs[documentcount].documenttype+"</div><div class='col'>"+thisPatientDocs[documentcount].documentname+"</div></div>"
        $('#patienDocList').append(documentRow);
        
        
    }
    
}

function getthisdocument(){
    
    
}

function selectPatients(){
    $('#selectpatientheading').slideToggle();
    $('#patientList').slideToggle();
}

function addscans(){
    $('#addDocs').slideToggle();
    
}

function hideshowdocs(){
    $('#selectdocumentheading').slideToggle();
    $('#patienDocList').slideToggle();
}